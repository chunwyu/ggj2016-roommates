LANDLORD SIMULATOR 2016

Made for GLOBAL GAME JAM 2016
(Theme: Ritual)


ABOUT
In Landlord Simulator 2016, you own an apartment, shown as a floorplan.
Your tenants, however, are an eclectic lot and are adamant about their
peculiarities. Some like to have toilet seats up, some down. Some like
the lights off, some like to leave them on. Whenever they see otherwise,
they become upset. Upset tenants will leave, dropping your apartment
rating.

Your only recourse is to flip the lights, toilet seats, and bed rolls
ahead of them as they wander around so they always see the perfect picture
of their world. Or lock the door so they don't see the mess, if that's your
choice.


DIRECTIONS
You start with one tenant. Each tenant has up to two quirks. After starting,
tenants will wander around the apartment, stopping by in rooms. Whenever they
pass a room and see that the furniture is in a state that does not fit their
quirk, they become less happy. 

You may click on beds, lamps, and toilets to flip their state. You should do
so ahead of your tenants as they wander the apartment.

You may also click on doors to close and lock them. Tenants may navigate around
locked doors to their destination if possible. If they are stuck in a single room,
they also become less happy.

Every midnight, existing tenants are charged rent, and a new tenant joins the
apartment, for up to 6 tenants.

A tenant's happiness cannot be restored. When tenants are upset more than 5 
times, they will leave, which drops your apartment rating by 1 star out of 5.
When your rating reaches 0, the game is over.



Credits (alphabetical):
J. SHAGAM
Music and Sound Design
http://beesbuzz.biz
http://metronomic.tk
http://sockpuppet.us

NATE SLOTTOW
UI Design and Programming
Icon Art
Unity Integration

CHUN YU
Engine Programmer
Title Art
Map Design and Art