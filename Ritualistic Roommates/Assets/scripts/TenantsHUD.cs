﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TenantsHUD : MonoBehaviour
{
	public static TenantsHUD instance;

	public int maxTenantHuds = 8;

	private List<TenantHUD> activeHuds = new List<TenantHUD>();

	/// Visual indices available in the UI panel at the right of the screen 
	private List<int> availableHudIndices = new List<int>();

	private static GameObject tenantHudPrefab;

	void Awake()
	{
		if (tenantHudPrefab == null)
		{
			tenantHudPrefab = Resources.Load<GameObject>("TenantHUD");
		}

		for (int i = 0; i < maxTenantHuds; i++)
		{
			availableHudIndices.Add(maxTenantHuds - i - 1);
		}

		instance = this;
	}

	public void AddHUD(Tenant tenant)
	{
		if (availableHudIndices.Count == 0)
		{
			Debug.LogError("Not enough space in the UI to add a new tenant hud");
			return;
		}


		int index = availableHudIndices[availableHudIndices.Count - 1];
		availableHudIndices.Remove(index);

		var go = Instantiate(tenantHudPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		var rectTransform = go.GetComponent<RectTransform>();
		rectTransform.SetParent(transform, worldPositionStays: false);

		var anchoredPos = rectTransform.anchoredPosition;
		anchoredPos.y = -rectTransform.sizeDelta.y * 0.5f + rectTransform.sizeDelta.y * (-index);
		rectTransform.anchoredPosition = anchoredPos;

		var hud = go.GetComponent<TenantHUD>();
		hud.index = index;

		hud.tenant = tenant;
		activeHuds.Add(hud);
	}

	public void RemoveHUD(TenantHUD hud)
	{
		activeHuds.Remove(hud);
		availableHudIndices.Add(hud.index);

		Destroy(hud.gameObject);
	}
}
