﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Navigation {
	const int UNVISITED = -1;

	public static Stack<Waypoint> shortestPath(List<Waypoint> graph, Waypoint a, Waypoint b)
	{
		if (a == null || b == null || a == b)
		{
			return null;
		}

		Queue<Waypoint> q = new Queue<Waypoint>();
		List<Waypoint> explored = new List<Waypoint>();
		Dictionary<Waypoint, int> d = new Dictionary<Waypoint, int>();
		Dictionary<Waypoint, Waypoint> prev = new Dictionary<Waypoint, Waypoint>();

		foreach (Waypoint w in graph)
		{
			d.Add(w, UNVISITED);
			prev.Add(w, null);
		}
		q.Enqueue(a);

		while (q.Count > 0)
		{
			Waypoint curr = q.Dequeue();
			explored.Add(curr);

			foreach (Waypoint n in curr.Neighbors)
			{
				if (n.IsDoor && !n.IsOpen)
				{
					continue;
				}

				if (n == b)
				{
					prev[b] = curr;
					Waypoint i = b;

					// we're done, start tracing path
					Stack<Waypoint> path = new Stack<Waypoint>();
					while(prev[i] != null)
					{
						path.Push(i);
						i = prev[i];
					}

					return path;
				}

				int dist = d[curr] + 1;
					
				if (!explored.Contains(n) || d[n] < dist)
				{
					d[n] = dist;
				}

				if (!explored.Contains(n))
				{
					if (n.IsDoor)
					{
						if (n.IsOpen)
						{
							q.Enqueue(n);
							prev[n] = curr;
						}
					}
					else
					{
						q.Enqueue(n);
						prev[n] = curr;
					}
				}
			}
		}

		return null;
	}
}
