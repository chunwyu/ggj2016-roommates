﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameClockDisplay : MonoBehaviour {

	public static GameClockDisplay instance;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	void Awake()
	{
		instance = this;
	}

	public void UpdateDisplay(float secondsLeftInDay, float secondsPerDay, int dayCount)
	{
		int hour = (int)((secondsPerDay - secondsLeftInDay) / secondsPerDay * 24);

		string output = "DAY " + (dayCount + 1) + " ";
		output += ((hour % 12 == 0) ? 12 : (hour % 12)) + ":00 " + (hour > 11 ? "PM" : "AM");

		Text t = gameObject.GetComponent<Text>();
		t.text = output;
	}
}
