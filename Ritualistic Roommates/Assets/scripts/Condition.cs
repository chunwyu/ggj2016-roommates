﻿using UnityEngine;
using System.Collections;

public class Condition : MonoBehaviour {

	public ConditionData data;

	// Use this for initialization
	void Start () {
		
	}
	
	public void init (ConditionData d, Vector2 position, int initialState)
	{
		data = d;
		gameObject.transform.position = position;
		d.CurrentState = initialState;

		RefreshSprite();
	}

	void RefreshSprite()
	{
		SpriteRenderer renderer = gameObject.GetComponent<SpriteRenderer>();
		renderer.sprite = data.StateImages[data.CurrentState];
	}
	
	void PlaySound()
	{
		AudioSource audioS = gameObject.GetComponent<AudioSource>();
		AudioClip clip = Resources.Load("sounds/" + data.StateSounds[data.CurrentState]) as AudioClip;
		audioS.PlayOneShot(clip, 0.8f);
	}

	public void Toggle()
	{
		int numStates = data.StateImages.Count;
		if (data.CurrentState == numStates - 1)
		{
			data.CurrentState = 0;
		}
		else
		{
			data.CurrentState += 1;
		}
		PlaySound();
		RefreshSprite();
	}

	public void Clicked()
	{
		Toggle();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
