﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StarRatingUI : MonoBehaviour
{
	public Color activeColor = Color.yellow;
	public Color inactiveColor = Color.gray;

	private Image[] m_starImages;

	private int m_stars;
	public int stars
	{
		get { return m_stars; }
		set
		{
			if (m_stars != value)
			{
				m_stars = value;

				for (int i = 0; i < m_starImages.Length; i++)
				{
					m_starImages[i].color = m_stars > i ? activeColor : inactiveColor;
				}
			}
		}
	}

	void Awake()
	{
		m_starImages = GetComponentsInChildren<Image>();
	}
}
