﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameOverPanel : MonoBehaviour
{
	public void OnRetry()
	{
		SceneManager.LoadScene("gameScene");
	}

	public void OnQuit()
	{
		SceneManager.LoadScene("titleScene");
	}
}
