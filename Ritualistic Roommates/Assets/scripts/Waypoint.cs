﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Waypoint
{
	public Waypoint(string name, float x, float y, bool isD, bool isO, bool isR)
	{
		this.name = name;
		this.x = x;
		this.y = y;
		isDoor = isD;
		IsOpen = isO;
		isRoom = isR;
		Conditions = new List<Condition>();
	}

	private string name;
	public string Name
	{
		get
		{
			return name;
		}
	}

	private float x;
	public float X
	{
		get
		{
			return x;
		}
	}

	private float y;
	public float Y
	{
		get
		{
			return y;
		}
	}

	public List<Waypoint> Neighbors
	{
		get;
		set;
	}

	public List<Condition> Conditions
	{
		get;
		set;
	}

	private bool isDoor;
	public bool IsDoor
	{
		get
		{
			return isDoor;
		}
	}

	private bool isRoom;
	public bool IsRoom
	{
		get
		{
			return isRoom;
		}
	}

	public bool IsOpen
	{
		get;
		set;
	}

	public void ToggleIsOpen()
	{
		IsOpen = !IsOpen;
		//Debug.Log("Door waypoint IsOpen set to " + IsOpen);
	}
}