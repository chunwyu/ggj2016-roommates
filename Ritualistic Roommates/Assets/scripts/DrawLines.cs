﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Put this script on a Camera
public class DrawLines : MonoBehaviour
{
	// Fill/drag these in from the editor

	// Choose the Unlit/Color shader in the Material Settings
	// You can change that color, to change the color of the connecting lines
	public Material lineMat;

	public List<Waypoint> waypoints;

	// Connect all of the `points` to the `mainPoint`
	void DrawConnectingLines()
	{
		if (waypoints != null && waypoints.Count > 0)
		{
			// Loop through each point to connect to the mainPoint
			foreach (Waypoint point in waypoints)
			{
				Vector2 mainPointPos = new Vector2(point.X, point.Y);

				foreach (Waypoint n in point.Neighbors)
				{
					Vector2 neighborPos = new Vector2(n.X, n.Y);

					GL.Begin(GL.LINES);
					lineMat.SetPass(0);
					GL.Color(new Color(lineMat.color.r, lineMat.color.g, lineMat.color.b, lineMat.color.a));
					GL.Vertex3(mainPointPos.x, mainPointPos.y, 0);
					GL.Vertex3(neighborPos.x, neighborPos.y, 0);
					GL.End();
				}
			}
		}
	}

	// To show the lines in the game window whne it is running
	void OnPostRender()
	{
		DrawConnectingLines();
	}

	// To show the lines in the editor
	void OnDrawGizmos()
	{
		DrawConnectingLines();
	}
}

