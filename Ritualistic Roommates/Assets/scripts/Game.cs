﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour {
	Map map;
	public GameObject tenantObject;
	Dictionary<string, ConditionData> conditions;
	List<List<Quirk>> quirks;

	List<Tenant> tenants;

	public float secondsPerDay = 20f;
	public float secondsLeftInDay;
	public int dayCount;

	int maxQuirksPerTenant;

	int money;		// score for now

	StarRatingUI ratingUI;

	bool gameOver;
	public GameOverPanel gameOverPanel;

	string[] names = { "Molly", "Jason", "Kevin", "Meng", "David", "Anthony" };

	// Use this for initialization
	void Start()
	{
		map = gameObject.GetComponent<Map>();

		constructQuirksConditions();

		constructMap();

		//DrawLines dl = Camera.main.gameObject.GetComponent<DrawLines>();
		//dl.waypoints = map.waypoints;

		constructTenants();

		money = 0;
		dayCount = 0;

		// Start the first day at midday to ramp up faster
		secondsLeftInDay = secondsPerDay / 2;

		UpdateMoneyDisplay();

		// Initialize the rating UI
		ratingUI = GameObject.FindObjectOfType<StarRatingUI>();
		ratingUI.stars = 5;
	}

	void constructTenant(int i, int maxQuirks)
	{
		GameObject testChar = (GameObject)Instantiate(tenantObject, new Vector3(0f, 0f, -1f), Quaternion.identity) as GameObject;
		Tenant testCharData = testChar.GetComponent<Tenant>();
		testCharData.gameMap = map;
		testCharData.tenantName = names[i];
		testCharData.currLocation = map.rooms[Random.Range(0, map.rooms.Count)];

		List<Quirk> quirkGen = new List<Quirk>();
		List<List<Quirk>> explored = new List<List<Quirk>>();

		// Pick quirks. Quirks are grouped in sets to ensure competing/opposing quirks
		// aren't picked for the same Tenant.
		while (quirkGen.Count < maxQuirks)
		{
			List<Quirk> quirkSet = quirks[Random.Range(0, quirks.Count)];
			if (!explored.Contains(quirkSet))
			{
				explored.Add(quirkSet);

				quirkGen.Add(quirkSet[Random.Range(0, quirkSet.Count)]);
			}
		}

		testCharData.init((int)i, quirkGen, 100);

		testCharData.Happiness = 5;
		testCharData.Speed = Random.Range(80, 150);

		// Hook into the tenant's destruct event
		testCharData.onDestruct.AddListener(() =>
		{
			tenants.Remove(testCharData);
			ratingUI.stars -= 1;
			if (ratingUI.stars == 0)
			{
				gameOver = true;
				gameOverPanel.gameObject.SetActive(true);
			}
		});

		// Create a HUD for the tenant
		TenantsHUD.instance.AddHUD(testCharData);
		tenants.Add(testCharData);
	}

	void constructTenants()
	{
		tenants = new List<Tenant>();

		TextAsset jsonData = Resources.Load("sample-apt") as TextAsset;
		string json = jsonData.text;
		JSONObject obj = new JSONObject(json);

		JSONObject tenantsJSON = obj["starting-tenants"];
		maxQuirksPerTenant = (int)(obj["tenant-maxquirk"].i);

		for (long i = 0; i < tenantsJSON.i; i++)
		{
			constructTenant((int)i, maxQuirksPerTenant);
		}
	}
	
	void constructWaypoints(JSONObject wpListJSON)
	{
		// make waypoints
		foreach (JSONObject wpJSON in wpListJSON.list)
		{
			Waypoint newW = new Waypoint(wpJSON["name"].str, wpJSON["x"].f, wpJSON["y"].f, wpJSON["door"].b, false, wpJSON["room"].b);
			map.waypointGraph.Add(wpJSON["name"].str, newW);
			map.waypoints.Add(newW);

			if (newW.IsDoor)
			{
				map.doors.Add(newW);
			}

			if (newW.IsRoom)
			{
				map.rooms.Add(newW);
			}
		}
	}

	void constructNeighbors(JSONObject wpListJSON)
	{
		// Make neighbors
		foreach (JSONObject wpJSON in wpListJSON.list)
		{
			map.waypointGraph[wpJSON["name"].str].Neighbors = new List<Waypoint>();

			foreach (JSONObject wpNeighJSON in wpJSON["neighbors"].list)
			{
				if (!map.waypointGraph.ContainsKey(wpJSON["name"].str))
				{
					Debug.Log("WARNING: CANNOT FIND " + wpJSON["name"].str + " as a source");
				}

				if (!map.waypointGraph.ContainsKey(wpNeighJSON.str))
				{
					Debug.Log("WARNING: CANNOT FIND " + wpNeighJSON.str + " as a neighbor");
				}
				map.waypointGraph[wpJSON["name"].str].Neighbors.Add(map.waypointGraph[wpNeighJSON.str]);
			}
		}
	}

	void constructConditionObjects(JSONObject condListJSON)
	{
		foreach (JSONObject cJSON in condListJSON.list)
		{
			GameObject newCond = (GameObject)Instantiate(
									Resources.Load("Condition", typeof(GameObject)),
									new Vector3(cJSON["x"].f, cJSON["y"].f, -1f),
									Quaternion.identity) as GameObject;
			Condition c = newCond.GetComponent<Condition>();

			string name = cJSON["name"].str;
			ConditionData orig = conditions[name];
			c.init((ConditionData)(orig.Clone()), new Vector2(cJSON["x"].f, cJSON["y"].f), (int)cJSON["startingState"].i);

			if (!map.waypointGraph.ContainsKey(cJSON["room"].str))
			{
				Debug.Log("WARNING: FAILED TO PLACE " + cJSON["name"].str + " in " + cJSON["room"].str);
			}
			map.waypointGraph[cJSON["room"].str].Conditions.Add(c);
		}
	}

	void constructDoors(JSONObject doorListJSON)
	{
		foreach (JSONObject dJSON in doorListJSON.list)
		{
			string hstr = dJSON["hinge"].str;

			GameObject newDoor = (GameObject)Instantiate(
									Resources.Load("DoorHinge" + hstr, typeof(GameObject))) as GameObject;
			newDoor.transform.position = new Vector3(dJSON["x"].f, dJSON["y"].f, -28f);

			string doorname = dJSON["name"].str;

			if (!map.waypointGraph.ContainsKey(doorname))
			{
				Debug.Log("WARNING: FAILED TO PLACE DOOR IN " + doorname);
			}
			else
			{
				bool open = true;

				Waypoint wp = map.waypointGraph[doorname];
				if (!wp.IsDoor)
				{
					Debug.Log("THIS ISN'T A DOOR");
				}

				var door = newDoor.GetComponent<Door>();
				door.wpHost = wp;
				door.OnOpen.AddListener(wp.ToggleIsOpen);
				door.OnClose.AddListener(wp.ToggleIsOpen);
				door.swingRight = dJSON["swingRight"].b;
				door.open = open;

				wp.IsOpen = open;
			}
		}
	}

	void constructMap()
	{
		TextAsset jsonData = Resources.Load("sample-apt") as TextAsset;
		string json = jsonData.text;

		JSONObject obj = new JSONObject(json);

		JSONObject wpListJSON = obj["wayPoints"];
		constructWaypoints(wpListJSON);
		constructNeighbors(wpListJSON);

		// Make condition objects
		JSONObject condListJSON = obj["conditions"];
		constructConditionObjects(condListJSON);

		// Make doors
		JSONObject doorListJSON = obj["doors"];
		constructDoors(doorListJSON);
    }

	void constructQuirksConditions()
	{
		TextAsset jsonData = Resources.Load("conditions-quirks") as TextAsset;
		string json = jsonData.text;

		JSONObject obj = new JSONObject(json);

		JSONObject whoop = obj["whoop"];
		JSONObject qListJSON = obj["quirks"];
		JSONObject condListJSON = obj["conditions"];

		conditions = new Dictionary<string, ConditionData>();
		quirks = new List<List<Quirk>>();

		// make condition data
		foreach (JSONObject cJSON in condListJSON.list)
		{
			List<Sprite> spr = new List<Sprite>();
			List<string> audiopaths = new List<string>();
			foreach (JSONObject stJSON in cJSON["states"].list)
			{
				Sprite s = Resources.Load("sprites/" + stJSON["image"].str, typeof(Sprite)) as Sprite;
				spr.Add(s);

				audiopaths.Add(stJSON["sound"].str);
			}
			ConditionData c = new ConditionData(spr, 0, cJSON["name"].str, audiopaths);

			conditions.Add(cJSON["name"].str, c);
		}

		// make quirks
		foreach (JSONObject qsJSON in qListJSON.list)
		{
			List<Quirk> quirkSet = new List<Quirk>();

			foreach (JSONObject qJSON in qsJSON["variations"].list)
			{
				Sprite s = Resources.Load("sprites/" + qJSON["image"].str, typeof(Sprite)) as Sprite;

				Quirk q = new Quirk(conditions[qsJSON["condition"].str], (int)qJSON["state"].i, s, qJSON["name"].str);
				quirkSet.Add(q);
			}
			quirks.Add(quirkSet);
		}
	}
	
	// Update is called once per frame
	void Update () {
		// HACK
		if (gameOver)
		{
			return;
		}

		// Check for quitting
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene("titleScene");
		}

		CheckInput();

		secondsLeftInDay -= Time.deltaTime;
		if (secondsLeftInDay <= 0f)
		{
			DayChange();
			secondsLeftInDay = secondsPerDay;
		}

		GameClockDisplay.instance.UpdateDisplay(secondsLeftInDay, secondsPerDay, dayCount);
	}

	int GetTotalRent()
	{
		int rate = 0;

		foreach (Tenant t in tenants)
		{
			rate += t.rent;
		}

		return rate;
	}

	void UpdateMoneyDisplay()
	{
		int rate = GetTotalRent();
		MoneyDisplay.instance.UpdateDisplay(money, rate);
	}

	void DayChange()
	{
		// Charge EXISTING tenants rent
		int rate = GetTotalRent();
        money += rate;

		int tenantnum = 0;

		// Add more tenants
		if (tenants.Count < 6)
		{
			bool found = false;
			while (!found)
			{
				int i = Random.Range(0, 6);
				bool unused = true;
				foreach(Tenant t in tenants)
				{
					if (t.tenantName == names[i])
					{
						unused = false;
					}
				}
				if (unused)
				{
					tenantnum = i;
					found = true;
					break;
				}
			}
			constructTenant(tenantnum, maxQuirksPerTenant);
		}

		// Rent's been updated, recalculate for display
		UpdateMoneyDisplay();

		dayCount += 1;
	}

	void CheckInput()
	{
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit2D hit = new RaycastHit2D();

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			hit = Physics2D.Raycast(ray.origin, ray.direction, 200f, -1);
            if (hit.transform != null)
			{
				Condition c = hit.transform.gameObject.GetComponent<Condition>();

				if (c != null)
				{
					c.SendMessage("Clicked");
				}
				else
				{
					Door d = hit.transform.gameObject.GetComponent<Door>();
					if (d != null)
					{
						d.SendMessage("Clicked");
					}
					else
					{
						Debug.Log("Clicked on a non-Condition");
					}
				}
			}
		}

	}
}
