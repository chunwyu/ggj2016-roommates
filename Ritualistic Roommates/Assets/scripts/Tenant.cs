﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

enum TenantState
{
	MOVING = 1,
	STANDING = 2,
	WAITING = 3,
	LEAVING = 4,
	DESTRUCTING = 5,
	STUCK = 6,
	LEAVESTUCK = 7,
	START = 8
};

public class Tenant : MonoBehaviour {
	// preferences bools
	TenantState state;
	public Map gameMap;

	public string tenantName;
	public float x;
	public float y;
	public List<Quirk> quirks;

	public Waypoint currLocation;
	public Waypoint currDest;		// the current next waypoint to move to
	private Vector2 currDestV;
	public Waypoint currFinalDest;  // the current FINAL waypoint in the stack (most likely a room)
	Stack<Waypoint> navPoints;

	public int rent;

	public float pathGoalThreshold = 0.5f;

	public Color color = Color.white;
	private Emoter emoter;
	private Transform currentEmoticon;

	private bool[] navigationVisited;

	public UnityEvent onDestruct;

	// Was the last thing we checked something we liked?
	private bool appeased;

	public static readonly Color[] colors = new[]
	{
		Color.red,
		new Color(0.25f, 0.59f, 1f),	// lighter blue than Color.blue
		new Color(0f, 0.77f, 0f),		// lighter green than Color.green
		new Color(1f, 0.87f, 0f),		// yellow, easier to see
		new Color(0.77f, 0f, 0.53f),	// purple
		new Color(1f, 0.5f, 0f)			// orange
	};

	void Awake()
	{
		emoter = GetComponent<Emoter>();
	}

	// Use this for initialization
	void Start () {
		state = TenantState.START; // not STANDING, don't want tenant to unfairly evaluate starting location
		navPoints = new Stack<Waypoint>();
	}

	public void init(int colorIndex, List<Quirk> qL, int r)
	{
		transform.position = new Vector2(currLocation.X, currLocation.Y);

		SpriteRenderer spr = gameObject.GetComponent<SpriteRenderer>();
		spr.sprite = Resources.Load<Sprite>("sprites/obj-tenant");
		spr.color = colors[colorIndex];
		color = spr.color;

		quirks = new List<Quirk>();

		foreach (Quirk q in qL)
		{
			quirks.Add(q);
		}

		rent = r;

		navigationVisited = new bool[gameMap.rooms.Count];
	}

	void CheckRoomForQuirks()
	{
		if (currLocation.IsRoom)
		{
			foreach (Condition c in currLocation.Conditions)
			{
				foreach (Quirk q in quirks)
				{
					var d = c.data;
					if (q.CondType.Name == d.Name)
					{
						if (q.DesiredState != d.CurrentState)
						{
							TenantQuirkViolation(q);
							//Debug.Log(tenantName + " saw that " + c.data.Name + " which violates her quirk of " + q.Name + "!!");
						}
						else
						{
							TenantQuirkAppeasement(q);
						}
					}
				}
			}
		}
	}

	// Update is called once per frame
	void Update () {
		switch(state)
		{
			case TenantState.START:
				StartCoroutine(WaitStart());
				break;
			case TenantState.MOVING:
				if (IsAtLocation(currDestV))
				{
					currLocation = currDest;

					CheckRoomForQuirks();
					// Don't overwrite DESTRUCTING state if we get into it.
					if (state == TenantState.DESTRUCTING)
					{
						break;
					}

					if (navPoints.Count == 0)
					{
						// done moving
						currDest = null;
						currFinalDest = null;

						state = TenantState.STANDING;

						//Debug.Log(tenantName + "going to STANDING (done moving)");

						break;
					}
					else
					{
						currDest = navPoints.Pop();
						currDestV = new Vector2(currDest.X, currDest.Y);

						// Tackling the case where the player closes a door on a tenant who
						// is pathing through it. Tenant should repath and, if not possible,
						// incur a small penalty and go elsewhere.
						if (currDest.IsDoor && !currDest.IsOpen)
						{
							navPoints.Clear();
							currDest = null;

							navPoints = Navigation.shortestPath(gameMap.waypoints, currLocation, currFinalDest);

							if (navPoints != null)
							{
								currDest = navPoints.Pop();
								currDestV = new Vector2(currDest.X, currDest.Y);
								break;
							}
							else
							{
								currDest = null;
								currFinalDest = null;

								// Play an annoyed animation here
								Emote("exclaimation");

								// Incur small penalty to player here for shutting out a tenant

								state = TenantState.STANDING;
								//Debug.Log(tenantName + "going to STANDING (locked out)");
								break;
							}
						}
					}
				}
				transform.position = Vector2.MoveTowards(transform.position, currDestV, Time.deltaTime * Speed);
				DrawPath();

				break;
			case TenantState.STANDING:
				// clear visited
				for (int i = 0; i < gameMap.rooms.Count; i++)
				{
					navigationVisited[i] = false;
				}

				StartCoroutine(WaitForMove());
				break;

			case TenantState.WAITING:
				break;

			case TenantState.LEAVING:
				//Debug.Log(tenantName + "In LEAVING");
				
				Waypoint destination = currLocation;
				int randomRoomIndex = 0;

				for (int i = 0; i < gameMap.rooms.Count; i++)
				{
					if (gameMap.rooms[i] == destination)
					{
						navigationVisited[i] = true;
						break;
					}
				}

				while (destination == currLocation)
				{
					randomRoomIndex = Random.Range(0, gameMap.rooms.Count);
                    destination = gameMap.rooms[randomRoomIndex];
				}

				//Debug.Log(tenantName + " is navigating from " + currLocation.Name + " to " + destination.Name);
				navigationVisited[randomRoomIndex] = true;
				navPoints = Navigation.shortestPath(gameMap.waypoints, currLocation, destination);

				if (navPoints != null)
				{
					state = TenantState.MOVING;
					currFinalDest = destination;
					currDest = navPoints.Pop();
					currDestV = new Vector2(currDest.X, currDest.Y);

					// clear visited
					for (int i = 0; i < gameMap.rooms.Count; i++)
					{
						navigationVisited[i] = false;
					}
				}
				else
				{
					Debug.Log("WARNING: " + tenantName + " FAILED TO FIND PATH FOR " + destination.Name);

					// Is the tenant stuck in a room?
					bool allTried = true;
					for (int i = 0; i < gameMap.rooms.Count; i++)
					{
						if (!navigationVisited[i])
						{
							allTried = false;
						}
					}

					// Yes he is.
					if (allTried)
					{
						// Penalize
						TenantQuirkViolation(null);

						// Don't overwrite DESTRUCTING state if we get into it.
						if (state == TenantState.DESTRUCTING)
						{
							break;
						}

						state = TenantState.STUCK;
					}
				}

				break;
			case TenantState.DESTRUCTING:
				if (currentEmoticon == null)
				{
					onDestruct.Invoke();
					Destroy(gameObject);
				}
				break;

			case TenantState.STUCK:
				Debug.Log(tenantName + " IS STUCK!");

				// Wait for a little bit, then try navigating again 
				StartCoroutine(WaitStuck());

				break;

			case TenantState.LEAVESTUCK:
				// clear visited
				for (int i = 0; i < gameMap.rooms.Count; i++)
				{
					navigationVisited[i] = false;
				}

				// check room for violation
				CheckRoomForQuirks();

				// Don't overwrite DESTRUCTING state if we get into it.
				if (state == TenantState.DESTRUCTING)
				{
					break;
				}

				Debug.Log(tenantName + " will try LEAVING again.");
				state = TenantState.LEAVING;
				break;
				
			default:
				break;
		}
	}

	public IEnumerator WaitForMove()
	{
		state = TenantState.WAITING;
		yield return new WaitForSeconds(1f);
		state = TenantState.LEAVING;
		//Debug.Log(tenantName + "going to LEAVING");
	}

	public IEnumerator WaitStuck()
	{
		state = TenantState.WAITING;
		yield return new WaitForSeconds(5f);
		state = TenantState.LEAVESTUCK;
	}

	public IEnumerator WaitStart()
	{
		state = TenantState.WAITING;
		yield return new WaitForSeconds(3f);
		state = TenantState.LEAVING;
	}

	// Called when a tenant's quirk is violated
	void TenantQuirkViolation(Quirk q)
	{
		Happiness -= 1;
		Speed += 20;

		if (Happiness <= 0)
		{
			// Set state to self-destructing and wait for the emoticon to get destroyed before destroying ourself.
			state = TenantState.DESTRUCTING;
		}

		// q == null means a violation due to tenant being stuck
		if (q == null)
		{
			AudioSource audioS = gameObject.GetComponent<AudioSource>();
			AudioClip clip = Resources.Load("sounds/sfx synth down 2") as AudioClip;
			audioS.PlayOneShot(clip, 0.8f);
			Emote("confused");
		}
		else
		{
			AudioSource audioS = gameObject.GetComponent<AudioSource>();
			AudioClip clip = Resources.Load("sounds/sfx angry grumble") as AudioClip;
            audioS.PlayOneShot(clip, 0.8f);
			Emote("ira");
		}

		appeased = false;
	}

	void TenantQuirkAppeasement(Quirk q)
	{
		//if (!appeased)
		{
			appeased = true;
			Emote("happy");
		}
	}

	void DrawPath()
	{
		LineRenderer lineRenderer = gameObject.GetComponent<LineRenderer>();
		lineRenderer.material = new Material(Shader.Find("Particles/Multiply"));
		lineRenderer.SetColors(color, color);
		lineRenderer.SetWidth(5f, 5f);

		lineRenderer.SetVertexCount(navPoints.Count + 2); // future nav points + curr nav point + curr location
		lineRenderer.SetPosition(0, transform.position);

		if (currDest != null)
		{
			lineRenderer.SetPosition(1, currDestV);
		}

		int i = 2;
		foreach (Waypoint w in navPoints)
		{
			Vector2 v = new Vector2(w.X, w.Y);
			lineRenderer.SetPosition(i, v);
			i++;
		}
	}

	void Emote(string emoticonName)
	{
		bool showFast = emoticonName == "ira";
		currentEmoticon = emoter.Emote(emoticonName, showFast);

		// Parent the emoticon to ourself so it moves with us
		currentEmoticon.SetParent(transform);
		var p = currentEmoticon.position;
		p.y += 80;
		p.z = -40;
		currentEmoticon.position = p;
	}

	bool IsAtLocation(Vector2 dest)
	{
		return (Vector2.Distance(transform.position, dest) < pathGoalThreshold);
	}

	public int Speed
	{
		get;
		set;
	}

	public int Happiness
	{
		get;
		set;
	}

}
