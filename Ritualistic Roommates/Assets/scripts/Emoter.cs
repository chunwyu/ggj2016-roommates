﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Emoter : MonoBehaviour
{
	private static GameObject emoticonPrefab;
	private static Sprite[] bubbleSprites;
	private static Dictionary<string, Sprite> iconSprites;

	void Awake()
	{
		if (emoticonPrefab == null)
		{
			emoticonPrefab = Resources.Load<GameObject>("Emoticon");
		}

		if (bubbleSprites == null)
		{
			bubbleSprites = new[]
			{
				Resources.Load<Sprite>("sprites/emote-bubble-01"),
				Resources.Load<Sprite>("sprites/emote-bubble-02"),
				Resources.Load<Sprite>("sprites/emote-bubble-03"),
			};
		}

		if (iconSprites == null)
		{
			iconSprites = new Dictionary<string, Sprite>();
			iconSprites.Add("exclaimation", Resources.Load<Sprite>("sprites/emote-exclaimation"));
			iconSprites.Add("ira", Resources.Load<Sprite>("sprites/emote-ira"));
			iconSprites.Add("confused", Resources.Load<Sprite>("sprites/emote-confused"));
			iconSprites.Add("happy", Resources.Load<Sprite>("sprites/emote-happy"));
		}
	}

	///
	/// Returns the emoticon's transform so you can make it a child of whomever is emoting
	///
	public Transform Emote(string name, bool showFast = false)
	{
		Sprite iconSprite;
		if (!iconSprites.TryGetValue(name, out iconSprite))
		{
			Debug.LogError("Couldn't find emoticon named: " + name);
			return null;
		}

		var go = Instantiate(emoticonPrefab, transform.position, Quaternion.identity) as GameObject;
		var emoticon = go.GetComponent<Emoticon>();
		emoticon.bubbleSprite = bubbleSprites[Random.Range(0, bubbleSprites.Length)];
		emoticon.iconSprite = iconSprite;
		emoticon.showFast = showFast;

		return emoticon.transform;
	}
}
