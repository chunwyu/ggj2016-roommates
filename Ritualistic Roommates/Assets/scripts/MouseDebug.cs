﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class MouseDebug : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		/*
		Text t = gameObject.GetComponent<Text>();
		t.text = GetDebugString();
		*/

		//ClickCoordinates();
	}

	void ClickCoordinates()
	{
		if (Input.GetMouseButtonDown(0))
		{
			Vector3 hit = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Debug.Log(String.Format("{0},{1}", hit.x, hit.y));
		}
	}

	string GetDebugString()
	{
		Vector3 hit = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		return String.Format("{0},{1}", hit.x, hit.y);
	}
}
