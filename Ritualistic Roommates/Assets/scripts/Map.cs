﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map : MonoBehaviour {

	public List<Waypoint> doors;
	public List<Waypoint> rooms;
	public List<Waypoint> waypoints;
	public Dictionary<string, Waypoint> waypointGraph;

	// Use this for initialization
	void Awake()
	{
		waypointGraph = new Dictionary<string, Waypoint>();
		doors = new List<Waypoint>();
		rooms = new List<Waypoint>();
		waypoints = new List<Waypoint>();


	}

	// Update is called once per frame
	void Update()
	{
	}


}
