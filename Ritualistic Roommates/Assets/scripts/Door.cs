﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Door : MonoBehaviour
{
	public UnityEvent OnOpen;
	public UnityEvent OnClose;

	public Waypoint wpHost;

	public bool open
	{
		get { return animator.GetBool("open"); }
		set
		{
			bool isOpen = open;
			if (isOpen != value)
			{
				animator.SetBool("open", value);

				if (value)
				{
					OnOpen.Invoke();
				}
				else
				{
					OnClose.Invoke();
				}
				PlaySound(value);
			}
		}
	}

	public bool swingRight
	{
		get { return animator.GetBool("swingRight"); }
		set
		{
			var current = swingRight;
			if (current != value)
			{
				animator.SetBool("swingRight", value);
			}
		}
	}

	private Animator m_animator;

	private Animator animator
	{
		get
		{
			if (m_animator == null)
			{
				m_animator = GetComponent<Animator>();
			}
			return m_animator;
		}
	}

	public void Clicked()
	{
		open = !open;
	}

	void PlaySound(bool open)
	{
		string path = (open) ? "sfx door open solo crush" : "sfx door lock solo crush";

		AudioSource audioS = gameObject.GetComponent<AudioSource>();
		AudioClip clip = Resources.Load("sounds/" + path) as AudioClip;
		audioS.PlayOneShot(clip, 0.8f);
	}
}
