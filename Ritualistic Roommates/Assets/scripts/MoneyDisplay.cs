﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoneyDisplay : MonoBehaviour
{

	public static MoneyDisplay instance;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
	}

	void Awake()
	{
		instance = this;
	}

	public void UpdateDisplay(int money, int rate)
	{
		string output = "$" + money + " ";
		output += "(+$" + rate + "/day)";

		Text t = gameObject.GetComponent<Text>();
		t.text = output;
	}
}
