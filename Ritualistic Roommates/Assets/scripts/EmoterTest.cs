﻿using UnityEngine;
using System.Collections;

public class EmoterTest : MonoBehaviour
{
	public float emotePeriod;
	float lastEmoteTime;

	Emoter emoter;

	static string[] emotes = new[] {"exclaimation", "happy", "ira", "confused"};
	int emoteIndex;

	void Awake()
	{
		emoter = GetComponent<Emoter>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Time.time >= lastEmoteTime + emotePeriod)
		{
			var emoteName = emotes[emoteIndex];
			bool showFast = emoteName == "ira";

			emoter.Emote(emoteName, showFast);
			lastEmoteTime = Time.time;

			if (++emoteIndex == emotes.Length)
			{
				emoteIndex = 0;
			}
		}
	}
}
