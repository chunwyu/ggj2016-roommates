﻿using UnityEngine;
using System.Collections;

public class Emoticon : MonoBehaviour
{
	public Sprite bubbleSprite
	{
		get { return bubbleSpriteRenderer.sprite; }
		set { bubbleSpriteRenderer.sprite = value; }
	}

	public Sprite iconSprite
	{
		get { return iconSpriteRenderer.sprite; }
		set { iconSpriteRenderer.sprite = value; }
	}

	public bool showFast
	{
		set { animator.SetBool("showFast", value); }
	}

	SpriteRenderer bubbleSpriteRenderer;
	SpriteRenderer iconSpriteRenderer;
	Animator animator;

	void Awake()
	{
		var bubbleTransform = transform.FindChild("Bubble");
		var iconTransform = transform.FindChild("Icon");

		bubbleSpriteRenderer = bubbleTransform.GetComponent<SpriteRenderer>();
		iconSpriteRenderer = iconTransform.GetComponent<SpriteRenderer>();

		animator = GetComponent<Animator>();
	}

	void Update()
	{
		if (animator.GetCurrentAnimatorStateInfo(0).IsName("Done"))
		{
			Destroy(gameObject);
		}
	}
}
