﻿using UnityEngine;
using System.Collections;

public class DoorTest : MonoBehaviour
{
	void Update ()
	{
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit2D hit = new RaycastHit2D();

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			hit = Physics2D.Raycast(ray.origin, ray.direction, 200f, -1);
			if (hit.transform != null)
			{
				var go = hit.transform.gameObject;
				var door = go.GetComponent<Door>();
				Debug.Log("Hit object", go);
				go.BroadcastMessage("Clicked");
			}
		}
	}
}
