﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConditionData : ICloneable
{
	List<Sprite> stateImage;
	List<string> stateSounds;
	int currState;
	string conditionName;

	public ConditionData(List<Sprite> sI, int startState, string conditionName, List<string> sS)
	{
		stateImage = sI;
		currState = startState;
		this.conditionName = conditionName;
		stateSounds = sS;
	}

	public object Clone()
	{
		return new ConditionData(stateImage, currState, conditionName, stateSounds);
	}

	public List<Sprite> StateImages
	{
		get
		{
			return stateImage;
		}
	}

	public List<string> StateSounds
	{
		get
		{
			return stateSounds;
		}
	}

	public int CurrentState
	{
		get;
		set;
	}

	public Sprite CurrentSprite
	{
		get
		{
			return stateImage[CurrentState];
		}
	}

	public string Name
	{
		get
		{
			return conditionName;
		}
	}
}


public class Quirk
{
	ConditionData condType;
	int desiredState;
	string quirkName;
	Sprite icon;

	public Quirk(ConditionData cT, int dS, Sprite ic, string qN)
	{
		condType = cT;
		desiredState = dS;
		icon = ic;
		quirkName = qN;
	}

	public ConditionData CondType
	{
		get
		{
			return condType;
		}
	}

	public int DesiredState
	{
		get
		{
			return desiredState;
		}
	}

	public Sprite Icon
	{
		get
		{
			return icon;
		}
	}

	public string Name
	{
		get
		{
			return quirkName;
		}
	}

	public bool QuirkCheck(ConditionData c)
	{
		// I know it looks ugly, I just need it to debug.
		if (c.Name == condType.Name)
		{
			return c.CurrentState == desiredState;
		}
		return true;
	}
}
