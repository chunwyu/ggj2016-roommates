﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TenantHUD : MonoBehaviour
{
	public int index;
	public bool deactivating;

	public Tenant tenant
	{
		get { return m_tenant; }
		set
		{
			if (m_tenant != value)
			{
				m_tenant = value;

				// Set the name
				nameText.text = m_tenant.tenantName;

				// Set the initial face color and mood
				faceBaseImage.color = m_tenant.color;
				faceMoodImage.sprite = faceMoodSprites[0];

				// Create quirks icons
				foreach (var quirk in m_tenant.quirks)
				{
					var iconGo = new GameObject();
					var iconTransform = iconGo.AddComponent<RectTransform>();
					var iconImage = iconGo.AddComponent<Image>();
					iconImage.sprite = quirk.Icon;
					iconImage.preserveAspect = true;

					iconTransform.SetParent(quirksGrid, worldPositionStays: false);
				}
			}
		}
	}

	Tenant m_tenant;
	Animator animator;

	public Text nameText;
	public Image faceBaseImage;
	public Image faceMoodImage;
	public RectTransform quirksGrid;

	private static Sprite[] faceMoodSprites;

	void Awake()
	{
		if (faceMoodSprites == null)
		{
			faceMoodSprites = new[]
			{
				Resources.Load<Sprite>("sprites/face-mood-01"),
				Resources.Load<Sprite>("sprites/face-mood-02"),
				Resources.Load<Sprite>("sprites/face-mood-03"),
				Resources.Load<Sprite>("sprites/face-mood-04"),
				Resources.Load<Sprite>("sprites/face-mood-05"),
				Resources.Load<Sprite>("sprites/face-mood-06")
			};

			// Hacky way to assert sprites loaded
			int index = 0;
			foreach (var sprite in faceMoodSprites)
			{
				if (sprite == null)
				{
					Debug.LogError("face mood sprite " + index + " is null");
				}
				index++;
			}
		}

		animator = GetComponent<Animator>();
	}

	public void Deactivate()
	{
		if (!deactivating)
		{
			animator.SetBool("hide", true);
			deactivating = true;
		}
	}

	void Update()
	{
		if (deactivating)
		{
			// Check if the hide animation has finished
			if (animator.GetCurrentAnimatorStateInfo(0).IsName("Done"))
			{
				// Free up the hud slot
				// NOTE: This destroys the game object
				TenantsHUD.instance.RemoveHUD(this);
				return;
			}
		}
		else if (tenant != null)
		{
			// Update the UI based on the tenant state
			int happiness = 5 - m_tenant.Happiness;
			int spriteIndex = happiness < 0 ?  0 :
				happiness >= faceMoodSprites.Length ?
					faceMoodSprites.Length - 1 : happiness;
			faceMoodImage.sprite = faceMoodSprites[spriteIndex];
		}
		else
		{
			// tenant == null (it was destroyed)
			Deactivate();
		}
	}
}
